import React from 'react';
import { useHistory } from 'react-router-dom';

const Home = () => {
  const history = useHistory();

  const handleClick = () => {
    history.push('/LivenessDemo'); // Redirect to the other function path
  };

  return (
    <div>
      <h1>Liveness V3 Demo</h1>
      <button onClick={handleClick}>Iniciar Liveness V3</button>
    </div>
  );
};

export default Home;