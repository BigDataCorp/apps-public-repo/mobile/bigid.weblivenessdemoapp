import '../../App';
import React from 'react';

class ResultPage extends React.Component {
  handleClick = () => {
    this.props.history.push('/'); // Redirect to the home page
  };

  render() {
    const { location } = this.props;
    let receivedData = location.state;

    // Fix double wrapping
    if (receivedData && receivedData.state) {
      receivedData = receivedData.state;
    }


    // Check if the received data is an error
    const isError = receivedData?.error;
    const status = receivedData?.score > receivedData?.threshold ? "Live person" : "not live person";

    return (
      <div>
        <h1>Liveness V3 Demo</h1>
        {isError ? (
          <div>
            <h3>Error:</h3>
            <p>{receivedData.error}</p>
          </div>
        ) : (
          <div>
            <h3>Status: {status || 'No status available'}</h3>
            <h3>Score: {receivedData?.score || 'No score available'}</h3>
            <h3>Message: {receivedData?.resultMessage}</h3>
            <h3>Result Code: {receivedData?.resultCode}</h3>
            <h3>{JSON.stringify(receivedData)}</h3>
            {receivedData?.auditImageBytes ? (
              <img 
                src={`data:image/jpeg;base64,${receivedData?.auditImageBytes}`} 
                alt="self image" 
              />
            ) : (
              <p>No image available</p>
            )}
          </div>
        )}
        <button onClick={this.handleClick}>Voltar</button>
      </div>
    );
  }
}

export default ResultPage;
