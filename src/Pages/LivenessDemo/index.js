import React from 'react';
import BigIdLivenessV3 from 'bigid_liveness_sdk';
import { withRouter } from 'react-router-dom';

const LivenessDemo = ({ history }) => {
  const handleFinish = (results) => {
    history.push('/ResultPage', { state: results });
  };

  return (
    <BigIdLivenessV3
      onFinish={handleFinish}
      token="your token here"
    />
  );
};

export default withRouter(LivenessDemo);
