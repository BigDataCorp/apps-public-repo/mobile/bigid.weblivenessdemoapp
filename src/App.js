import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './Pages/Home';
import LivenessDemo from './Pages/LivenessDemo';
import ResultPage from './Pages/ResultPage';

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/LivenessDemo" component={LivenessDemo} />
        <Route path="/ResultPage" component={ResultPage} />
      </Switch>
    </Router>
  );
};

export default App;
